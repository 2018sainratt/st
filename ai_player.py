from player import Player
from random import *


class AIPlayer(Player):
	"""This player should implement a heuristic along with a min-max and alpha
	beta search to """
	
	def __init__(self):
		self.name = "Mettez ici le nom de votre IA"

	
	def getColumn(self, board):
		l = board.getPossibleColumns()
		i = randint(len(l))
		return l[i]
